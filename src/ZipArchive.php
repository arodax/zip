<?php

/*
 * This file is part of the zip package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\Zip;

class ZipArchive extends AbstractZip
{
    public function save(string $target): void
    {
        $zip = new \ZipArchive();
        $zip->open($target,  \ZipArchive::OVERWRITE|\ZipArchive::CREATE);

        foreach ($this->paths as $path) {
            if (is_dir($path)) {
                $this->addDir($zip, $path);
            }
            else if (is_file($path)) {
                $zip->addFile($path);
            }
        }

        foreach ($this->data as $key => $val) {
            $zip->addFromString($key,  $val);
        }

        $zip->close();
    }

    private function addDir(\ZipArchive $zip, string $path): void
    {
        $dirName = basename($path);
        $parentPath = dirname($path);

        $zip->addEmptyDir($dirName);
        self::folderToZip($path, $zip, strlen("$parentPath/"));
    }

    private static function folderToZip(string $folder, \ZipArchive $zipFile, int $exclusiveLength): void
    {
        $handle = opendir($folder);
        while (false !== $f = readdir($handle)) {
            if ($f !== '.' && $f !== '..') {
                $filePath = "$folder/$f";
                // Remove prefix from file path before add to zip.
                $localPath = substr($filePath, $exclusiveLength);
                if (is_file($filePath)) {
                    $zipFile->addFile($filePath, $localPath);
                } elseif (is_dir($filePath)) {
                    // Add sub-directory.
                    $zipFile->addEmptyDir($localPath);
                    self::folderToZip($filePath, $zipFile, $exclusiveLength);
                }
            }
        }
        closedir($handle);
    }
}
