<?php

/*
 * This file is part of the zip package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\Zip;

interface ZipInterface
{
    public function save(string $target): void;
    public function clear(): self;
    public function addPath(string ...$paths): self;
    public function addData(string $data, string $name): self;
}
