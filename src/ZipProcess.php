<?php

/*
 * This file is part of the zip package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\Zip;

use Symfony\Component\Process\Process;

class ZipProcess extends AbstractZip
{
    public function __construct(
        private readonly string $tmpDir = '/tmp',
        private readonly bool $includePaths = false
    )
    {
        parent::__construct();
    }

    public function save(string $target): void
    {
        $paths = [];
        foreach ($this->data as $key => $val) {
            $filename = $this->tmpDir.'/'.$key;
            file_put_contents($filename, $val);
            $paths[] = $filename;
        }

        $paths = array_merge($paths, $this->paths->toArray());

        $process = ['zip', '-r'];
        if (! $this->includePaths) {
            $process[] = '-j';
        }
        $process = [...$process, $target, ...$paths];
        $process = new Process($process);
        $process->mustRun();
    }
}
