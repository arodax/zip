<?php

/*
 * This file is part of the zip package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\Zip;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

abstract class AbstractZip implements ZipInterface
{
    protected Collection $paths;
    protected Collection $data;

    public function __construct()
    {
        $this->paths = new ArrayCollection();
        $this->data = new ArrayCollection();
    }

    public function clear(): self
    {
        $this->paths->clear();
        $this->data->clear();

        return $this;
    }

    public function addPath(string ...$paths): self
    {
        foreach ($paths as $path) {
            if (!$this->paths->contains($path)) {
                $this->paths->add($path);
            }
        }

        return $this;
    }

    public function addData(string $data, string $name): self
    {
        $this->data->set($name, $data);

        return $this;
    }
}
